
function toggleClick() {
	var element = document.getElementsByClassName("cart__list");
	if (window.matchMedia("(max-width: 992px)").matches) {
		element[0].classList.toggle("none");
	}
	console.log(window.matchMedia("screen").matches);

}
function myTogg() {
	var element = document.getElementsByClassName("menu-btn");
	element[0].classList.toggle("active");
	var element1 = document.getElementsByClassName("header__menu-list");
	element1[0].classList.toggle("active");
}

function toggleInline() {
	var element = document.getElementsByClassName("product__item");
	for (let i = 0; i < element.length; i++) {
		element[i].classList.add("product__item-inline");
	}
	var element1 = document.getElementsByClassName("sort-by__show-inline");
	element1[0].classList.add("active");
	var element2 = document.getElementsByClassName("sort-by__show-grid");
	element2[0].classList.remove("active");
}
function toggleGrid() {
	var element = document.getElementsByClassName("product__item");
	for (let i = 0; i < element.length; i++) {
		element[i].classList.remove("product__item-inline");
	}
	var element1 = document.getElementsByClassName("sort-by__show-grid");
	element1[0].classList.add("active");
	var element2 = document.getElementsByClassName("sort-by__show-inline");
	element2[0].classList.remove("active");
}

function tabShowContent(str) {
	var elementContent1 = document.getElementById("tab-features");
	var elementContent2 = document.getElementById("tab-coments");
	var elementContent3 = document.getElementById("tab-review");
	var elementContent4 = document.getElementById("tab-support");

	var arr = [
		elementContent1,
		elementContent2,
		elementContent3,
		elementContent4
	];

	for (let i = 0; i < arr.length; i++) {

		if (str === arr[i].id) {
			console.log(arr[i]);
			arr[i].classList.add("tab-active");
			for (let j = 0; j < arr.length; j++) {
				if (arr[i] !== arr[j]) {
					arr[j].classList.remove("tab-active");
					arr[j].classList.remove("tab-active");
					arr[j].classList.remove("tab-active");
				}
			}

		}
	}
}

$(function () {

	$('select#type').styler();

});
$(function () {
	mixitup('.products__items', {
		animation: {
			duration: 300
		}
	});
});



$(function () {
	$('.products-slider__inner').slick({
		dots: true,
		infinite: true,
		arrows: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		mobileFirst: true
	});
});

$(function () {

	$(".product__author-rating").rateYo({
		rating: 3.6,
		starWidth: "11px"
	});

});

$(".js-range-slider").ionRangeSlider({
	prefix: " $"
});